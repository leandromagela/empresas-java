FROM adoptopenjdk/openjdk11
VOLUME /tmp
ARG JAR_FILE=target/empresas-java-0.0.1.jar
COPY ${JAR_FILE} EmpresasJavaApplication.jar
ENTRYPOINT ["java","-jar","/EmpresasJavaApplication.jar"]
