package com.ioasys.empresasjava.controller;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.ioasys.empresasjava.DTO.UserDTO;
import com.ioasys.empresasjava.service.UserService;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {
	
	@InjectMocks
	private UserController userController;
	
	@Mock
	private UserService userService;
	
	@Test
	public void getAllUsersTest() {
		UserDTO userDTO = new UserDTO();
		List<UserDTO> users = Collections.singletonList(userDTO);
		Mockito.when(userService.getAllUsers()).thenReturn(users);
		Mockito.when(userController.getAllUsers());
	}
	
	@Test
	public void getUsersNonAdministratorsActivesTest() {
		UserDTO userDTO = new UserDTO();
		List<UserDTO> users = Collections.singletonList(userDTO);
		Mockito.when(userService.getUsersNonAdministratorsActives()).thenReturn(users);
		Mockito.when(userController.getUsersNonAdministratorsActives());		
	}
	
	@Test
	public void findByIdTest() {
		UserDTO userDTO = new UserDTO();
		Long id = (long) 1;
		Mockito.when(userService.findById(id)).thenReturn(userDTO);
		Mockito.when(userController.findById(id));				
	}
	
	@Test
	public void saveNewUserTest() {
		UserDTO userDTO = new UserDTO();
		Mockito.when(userService.saveNewUser(userDTO)).thenReturn(userDTO);
		Mockito.when(userController.saveNewUser(userDTO));
	}
	
	@Test
	public void deleteTest() {
		UserDTO userDTO = new UserDTO();
		Long id = (long) 1;
		Mockito.when(userService.delete(id)).thenReturn(userDTO);
		Mockito.when(userController.delete(id));		
	}
	
	@Test
	public void queryByUsernameTest() {
		UserDTO userDTO = new UserDTO();
		List<UserDTO> users = Collections.singletonList(userDTO);
		Mockito.when(userService.queryByUsername("name")).thenReturn(users);
		Mockito.when(userController.queryByUsername("name"));
	}
	
	@Test
	public void updateTest() {
		UserDTO userDTO = new UserDTO();
		Mockito.when(userService.update(userDTO)).thenReturn(userDTO);
		Mockito.when(userController.update(userDTO));
	}

}
