package com.ioasys.empresasjava.controller;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.ioasys.empresasjava.DTO.VoteDTO;
import com.ioasys.empresasjava.service.VoteService;

@RunWith(MockitoJUnitRunner.class)
public class VoteControllerTest {

	@InjectMocks
	private VoteController voteController;

	@Mock
	private VoteService voteService;

	@Test
	public void getAllVotesTest() {
		VoteDTO voteDTO = new VoteDTO();
		List<VoteDTO> votes = Collections.singletonList(voteDTO);
		Mockito.when(voteService.getAllVotes()).thenReturn(votes);
		Mockito.when(voteController.getAllVotes());
	}

	@Test
	public void saveNewVoteTest() {
		VoteDTO voteDTO = new VoteDTO();
		Mockito.when(voteService.saveNewVote(voteDTO)).thenReturn(voteDTO);
		Mockito.when(voteController.saveNewVote(voteDTO));
	}

	@Test
	public void deleteTest() {
		VoteDTO voteDTO = new VoteDTO();
		Long id = (long) 1;
		Mockito.when(voteService.delete(id)).thenReturn(voteDTO);
		Mockito.when(voteController.delete(id));
	}

	@Test
	public void updateTest() {
		VoteDTO voteDTO = new VoteDTO();
		Mockito.when(voteService.update(voteDTO)).thenReturn(voteDTO);
		Mockito.when(voteController.update(voteDTO));
	}

}
