package com.ioasys.empresasjava.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.ioasys.empresasjava.DTO.MovieDTO;
import com.ioasys.empresasjava.service.MovieService;

@RunWith(MockitoJUnitRunner.class)
public class MovieControllerTest {

	@InjectMocks
	private MovieController movieController;

	@Mock
	private MovieService movieService;

	@Test
	public void getAllMoviesTest() {
		MovieDTO movieDTO = new MovieDTO();
		List<MovieDTO> movies = Collections.singletonList(movieDTO);
		Mockito.when(movieService.getAllMovies()).thenReturn(movies);
		Mockito.when(movieController.getAllMovies());
	}

	@Test
	public void saveNewMovieTest() {
		MovieDTO movieDTO = new MovieDTO();
		Mockito.when(movieService.saveNewMovie(movieDTO)).thenReturn(movieDTO);
		Mockito.when(movieController.saveNewMovie(movieDTO));
	}

	@Test
	public void getMoviesByFilterTest() {
		List<MovieDTO> movies = new ArrayList<>();
		Mockito.when(movieService.getMoviesByFilter("director", "title", "genre", "actor")).thenReturn(movies);
		Mockito.when(movieController.getMoviesByFilter("director", "title", "genre", "actor"));
	}

}
