/*create schema if not exists `empresas-java`;*/

/* create table user 
CREATE TABLE IF NOT EXISTS `empresas-java`.user (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `date_change` datetime DEFAULT NULL,
  `date_register` datetime DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1; */

-- `empresas-java`.`user` definition

CREATE TABLE TABLE IF NOT EXISTS `empresas-java`.user (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `date_change` datetime DEFAULT NULL,
  `date_register` datetime DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `vote_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKhlnhm5m7nerxvv9nfsbqemn8o` (`vote_id`),
  CONSTRAINT `FKhlnhm5m7nerxvv9nfsbqemn8o` FOREIGN KEY (`vote_id`) REFERENCES `vote` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*
CREATE TABLE  IF NOT EXISTS `empresas-java`.vote
(
  id Bigint NOT NULL AUTO_INCREMENT,
  rating Int NOT NULL,
  status Bool,
  date_register_vote Datetime,
  date_change_vote Datetime,
  id_movie Bigint NOT NULL,
  id_user Bigint NOT NULL,
 PRIMARY KEY (id,id_movie,id_user)
); */

-- `empresas-java`.vote definition

CREATE TABLE `vote` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rating` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `date_register_vote` datetime DEFAULT NULL,
  `date_change_vote` datetime DEFAULT NULL,
  `id_movie` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  PRIMARY KEY (`id`,`id_movie`,`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/* create table movie*/

/*
CREATE TABLE  IF NOT EXISTS `empresas-java`.movie
(
  id Bigint NOT NULL AUTO_INCREMENT,
  title Varchar(255) NOT NULL,
  description Varchar(255) NOT NULL,
  duration Int,
  `year` Int,
  director Varchar(255),
  actor Varchar(255),
  genre Varchar(255),
  country Varchar(255),
  status Bool,
  date_register_movie Datetime,
  date_change_movie Datetime,
   PRIMARY KEY (id)
); */

-- `empresas-java`.movie definition

CREATE TABLE `movie` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `director` varchar(255) DEFAULT NULL,
  `actor` varchar(255) DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `date_register_movie` datetime DEFAULT NULL,
  `date_change_movie` datetime DEFAULT NULL,
  `actors` varchar(255) DEFAULT NULL,
  `vote_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6sf3p3rxk8cxu1m73oc6gg5e3` (`vote_id`),
  CONSTRAINT `FK6sf3p3rxk8cxu1m73oc6gg5e3` FOREIGN KEY (`vote_id`) REFERENCES `vote` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


ALTER TABLE vote ADD CONSTRAINT movie_vote FOREIGN KEY (id_movie) REFERENCES movie (id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE vote ADD CONSTRAINT user_vote FOREIGN KEY (id_user) REFERENCES user (id) ON DELETE NO ACTION ON UPDATE NO ACTION;
