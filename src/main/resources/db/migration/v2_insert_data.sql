INSERT INTO `empresas-java`.`user`
(id, date_change, date_register, email, first_name, last_name, password, status, `type`, username, vote_id)
VALUES(1, NOW(), NOW(), 'ioasys@ioasys.com.br', 'Simon', 'Duggan', 'x4sfa24e', 1, 'ADMINISTRATOR', 'simonduggan', 1);


INSERT INTO `empresas-java`.movie
(id, title, description, duration, `year`, director, actor, genre, country, status, date_register_movie, date_change_movie, vote_id)
VALUES(1, 'Duro de Matar 4.0', 
'Enquanto a nação se prepara para comemorar o Dia da Independência, o policial veterano John McClane cumpre outra missão de rotina: trazer um "hacker" para ser interrogado. ', 
90, 2007, 'Len Wiseman', 'Roderick Thorp, Mark Bomback, David Marconi, John Carlin', 'Ação', 'EUA', 1, NOW(), NOW(), 1);

INSERT INTO `empresas-java`.vote
(id, rating, status, date_register_vote, date_change_vote, id_movie, id_user)
VALUES(id, 4, 1, NOW(), NOW(), 1, 1);