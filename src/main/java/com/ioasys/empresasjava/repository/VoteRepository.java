package com.ioasys.empresasjava.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ioasys.empresasjava.model.Vote;

public interface VoteRepository extends JpaRepository<Vote, Long> {
	
	Vote findById(long id);

}
