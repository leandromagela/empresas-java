package com.ioasys.empresasjava.repository;

import java.util.List;

import com.ioasys.empresasjava.model.Movie;

public interface MovieReportRepository {

	public List<Movie> getMoviesByFilter(String director, String title, String genre, String actor);
}
