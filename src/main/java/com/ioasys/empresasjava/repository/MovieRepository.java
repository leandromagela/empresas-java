package com.ioasys.empresasjava.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ioasys.empresasjava.model.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long>, MovieReportRepository {
	
	Movie findById(long id);
}
