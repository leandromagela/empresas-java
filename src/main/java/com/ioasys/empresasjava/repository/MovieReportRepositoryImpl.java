package com.ioasys.empresasjava.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.ioasys.empresasjava.model.Movie;

public class MovieReportRepositoryImpl implements MovieReportRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Movie> getMoviesByFilter(String director, String title, String genre, String actor) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT id, title, description, duration, year, "
				+ "director, actor, genre, country, status, date_register_movie, date_change_movie  ");
		sb.append("FROM Movie ");
		//sb.append("WHERE ");
		//sb.append("genre LIKE %:genre% ");

		if (director != null) {
			//sb.append("AND director LIKE %:director% ");
		}

		if (title != null) {
			//sb.append("AND title LIKE %:title% ");
		}

		if (actor != null) {
			//sb.append("AND actor LIKE %:actor% ");
		}

		Query query = entityManager.createQuery(sb.toString());
		query.setParameter("genre", genre);

		if (director != null) {
			query.setParameter("director", director);
		}

		if (title != null) {
			query.setParameter("title", title);
		}		

		if (actor != null) {
			query.setParameter("actor", actor);
		}

		return query.getResultList();

	}
}
