package com.ioasys.empresasjava.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ioasys.empresasjava.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findById(long id);

	List<User> queryByUsernameLike(String username);
}
