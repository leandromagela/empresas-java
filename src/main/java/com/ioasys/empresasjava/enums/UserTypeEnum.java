package com.ioasys.empresasjava.enums;

public enum UserTypeEnum {

	ADMINISTRATOR(1, "Administrator"), INTERNET_USER(2, "Internet User");

	private Integer userTypeId;
	private String userTypeName;

	UserTypeEnum(int userTypeId, String userTypeName) {
		this.userTypeId = userTypeId;
		this.userTypeName = userTypeName;
	}

	public Integer getUserTypeId() {
		return userTypeId;
	}

	public void setUserTypeId(Integer userTypeId) {
		this.userTypeId = userTypeId;
	}

	public String getUserTypeName() {
		return userTypeName;
	}

	public void setUserTypeName(String userTypeName) {
		this.userTypeName = userTypeName;
	}

	public static UserTypeEnum fromString(Integer value) {
		for (UserTypeEnum type : UserTypeEnum.values()) {
			if (type.getUserTypeId() == value) {
				return type;
			}
		}

		return null;
	}

}
