package com.ioasys.empresasjava.DTO;

import java.util.Date;

import com.ioasys.empresasjava.model.Vote;

public class MovieDTO {

	private Long id;
	private String title;
	private String description;
	private Integer duration;
	private Integer year;
	private String country;
	private String director;
	private String genre;
	private String actor;
	private Boolean status;
	private Date dateRegisterMovie;
	private Date dateChangeMovie;
	private Vote vote;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDirector() {
		return director;

	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getActor() {
		return actor;
	}

	public void setActor(String actor) {
		this.actor = actor;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getDateRegisterMovie() {
		return dateRegisterMovie;
	}

	public void setDateRegisterMovie(Date dateRegisterMovie) {
		this.dateRegisterMovie = dateRegisterMovie;
	}

	public Date getDateChangeMovie() {
		return dateChangeMovie;
	}

	public void setDateChangeMovie(Date dateChangeMovie) {
		this.dateChangeMovie = dateChangeMovie;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public Vote getVote() {
		return vote;
	}

	public void setVote(Vote vote) {
		this.vote = vote;
	}
}
