package com.ioasys.empresasjava.DTO;

import java.util.Date;
import java.util.List;

import com.ioasys.empresasjava.model.Movie;
import com.ioasys.empresasjava.model.User;

public class VoteDTO {

	private Long id;
	private Integer rating;
	private List<Movie> movie;
	private List<User> user;
	private Boolean status;
	private Date dateRegisterVote;
	private Date dateChangeVote;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public List<Movie> getMovie() {
		return movie;
	}

	public void setMovie(List<Movie> movie) {
		this.movie = movie;
	}

	public List<User> getUser() {
		return user;
	}

	public void setUser(List<User> user) {
		this.user = user;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getDateRegisterVote() {
		return dateRegisterVote;
	}

	public void setDateRegisterVote(Date dateRegisterVote) {
		this.dateRegisterVote = dateRegisterVote;
	}

	public Date getDateChangeVote() {
		return dateChangeVote;
	}

	public void setDateChangeVote(Date dateChangeVote) {
		this.dateChangeVote = dateChangeVote;
	}
}
