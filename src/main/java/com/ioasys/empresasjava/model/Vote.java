package com.ioasys.empresasjava.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ioasys.empresasjava.DTO.VoteDTO;

import lombok.Data;

@Data
@Entity
@Table(name = "vote")
public class Vote {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Integer rating;
	@OneToMany
	private List<Movie> movie;
	@OneToMany
	private List<User> user;
	private Boolean status;
	private Date dateRegisterVote;
	private Date dateChangeVote;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public List<Movie> getMovie() {
		return movie;
	}

	public void setMovie(List<Movie> movie) {
		this.movie = movie;
	}

	public List<User> getUser() {
		return user;
	}

	public void setUser(List<User> user) {
		this.user = user;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getDateRegisterVote() {
		return dateRegisterVote;
	}

	public void setDateRegisterVote(Date dateRegisterVote) {
		this.dateRegisterVote = dateRegisterVote;
	}

	public Date getDateChangeVote() {
		return dateChangeVote;
	}

	public void setDateChangeVote(Date dateChangeVote) {
		this.dateChangeVote = dateChangeVote;
	}

	public static Vote convert(VoteDTO voteDTO) {
		Vote vote = new Vote();
		vote.setId(voteDTO.getId());
		vote.setRating(voteDTO.getRating());
		vote.setMovie(voteDTO.getMovie());
		vote.setUser(voteDTO.getUser());
		vote.setStatus(voteDTO.getStatus());
		vote.setDateRegisterVote(voteDTO.getDateRegisterVote());
		vote.setDateChangeVote(voteDTO.getDateChangeVote());
		return vote;
	}

}
