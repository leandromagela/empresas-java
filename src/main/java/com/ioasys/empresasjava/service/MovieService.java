package com.ioasys.empresasjava.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ioasys.empresasjava.DTO.MovieDTO;
import com.ioasys.empresasjava.converter.DTOConverter;
import com.ioasys.empresasjava.model.Movie;
import com.ioasys.empresasjava.repository.MovieRepository;

@Service
public class MovieService {

	@Autowired
	private MovieRepository movieRepository;

	public List<MovieDTO> getAllMovies() {
		List<Movie> movies = movieRepository.findAll();
		return movies.stream().sorted(Comparator.comparing(Movie::getTitle)).map(DTOConverter::convertMovie)
				.collect(Collectors.toList());
	}

	public MovieDTO saveNewMovie(MovieDTO movieDTO) {
		Movie movie = movieRepository.save(Movie.convert(movieDTO));
		return DTOConverter.convertMovie(movie);
	}

	public List<MovieDTO> getMoviesByFilter(String director, String title, String genre, String actor) {
		List<Movie> movies = movieRepository.getMoviesByFilter(director, title, genre, actor);
		return movies.stream().map(DTOConverter::convertMovie).collect(Collectors.toList());
	}
}
