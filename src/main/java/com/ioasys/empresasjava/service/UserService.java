package com.ioasys.empresasjava.service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ioasys.empresasjava.DTO.UserDTO;
import com.ioasys.empresasjava.converter.DTOConverter;
import com.ioasys.empresasjava.exception.UserNotFoundException;
import com.ioasys.empresasjava.model.User;
import com.ioasys.empresasjava.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	public List<UserDTO> getAllUsers() {
		List<User> users = userRepository.findAll();
		return users.stream()
				.sorted(Comparator.comparing(User::getFirstName))
				.map(DTOConverter::convertUser).collect(Collectors.toList());
	}

	public List<UserDTO> getUsersNonAdministratorsActives() {
		List<User> users = userRepository.findAll();
		return users.stream()
				.filter(u -> u.getStatus().equals(Boolean.TRUE))
				.filter(u -> !u.getType().contains("ADMINISTRATOR"))
				.sorted(Comparator.comparing(User::getFirstName))
				.map(DTOConverter::convertUser).collect(Collectors.toList());
	}

	public UserDTO findById(long userId) {
		Optional<User> user = Optional.of(userRepository.findById(userId));
		if (user.isPresent()) {
			return DTOConverter.convertUser(user.get());
		}
		throw new UserNotFoundException();
	}

	public UserDTO saveNewUser(UserDTO userDTO) {
		User user = userRepository.save(User.convert(userDTO));
		return DTOConverter.convertUser(user);
	}
	
	public UserDTO delete(long userId) throws UserNotFoundException {
		User currentUser = userRepository.findById(userId);
		currentUser.setStatus(false);
		User user = userRepository.save(currentUser);
		return DTOConverter.convertUser(user);
	}
	
	public List<UserDTO> queryByUsername(String username) {
		List<User> users = userRepository.queryByUsernameLike(username);
		return users.stream().map(DTOConverter::convertUser).collect(Collectors.toList());		
	}

	public UserDTO update(UserDTO userDTO) {
		User user = userRepository.save(User.convert(userDTO));
		return DTOConverter.convertUser(user);
	}
}
