package com.ioasys.empresasjava.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ioasys.empresasjava.DTO.VoteDTO;
import com.ioasys.empresasjava.converter.DTOConverter;
import com.ioasys.empresasjava.exception.UserNotFoundException;
import com.ioasys.empresasjava.model.Vote;
import com.ioasys.empresasjava.repository.VoteRepository;

@Service
public class VoteService {

	@Autowired
	private VoteRepository voteRepository;
	
	public List<VoteDTO> getAllVotes() {
		List<Vote> votes = voteRepository.findAll();
		return votes.stream()
				.sorted(Comparator.comparing(Vote::getId))
				.map(DTOConverter::convertVote).collect(Collectors.toList());
	}
	
	public VoteDTO saveNewVote(VoteDTO VoteDTO) {
		Vote vote = voteRepository.save(Vote.convert(VoteDTO));
		return DTOConverter.convertVote(vote);
	}

	public VoteDTO delete(long voteId) throws UserNotFoundException {
		Vote currentVote = voteRepository.findById(voteId);
		currentVote.setStatus(false);
		Vote vote = voteRepository.save(currentVote);
		return DTOConverter.convertVote(vote);
	}
	
	public VoteDTO update(VoteDTO voteDTO) {
		Vote vote = voteRepository.save(Vote.convert(voteDTO));
		return DTOConverter.convertVote(vote);
	}
}
