package com.ioasys.empresasjava.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ioasys.empresasjava.DTO.UserDTO;
import com.ioasys.empresasjava.exception.UserNotFoundException;
import com.ioasys.empresasjava.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api")
@Api(value = "API REST to list, update, save and delete users")
public class UserController {

	@Autowired
	UserService userService;

	@ApiOperation(value = "Returns the list of all active or inactive users.")
	@GetMapping("/user/")
	public List<UserDTO> getAllUsers() {
		List<UserDTO> users = userService.getAllUsers();
		return users;
	}

	@ApiOperation(value = "Returns the list of active non-administrator users")
	@GetMapping("/user/listActiveNonAdministratorsUsers")
	public List<UserDTO> getUsersNonAdministratorsActives() {
		List<UserDTO> users = userService.getUsersNonAdministratorsActives();
		return users;
	}

	@ApiOperation(value = "Search a user by id")
	@GetMapping("/user/{id}")
	public UserDTO findById(@PathVariable Long id) {
		return userService.findById(id);
	}

	@ApiOperation(value = "Save a new user")
	@PostMapping("/user")
	public UserDTO saveNewUser(@RequestBody UserDTO userDTO) {
		return userService.saveNewUser(userDTO);
	}

	@ApiOperation(value = "Delete a user by id")
	@DeleteMapping("/user/{id}")
	public UserDTO delete(@PathVariable Long id) throws UserNotFoundException {
		return userService.delete(id);
	}

	@ApiOperation(value = "Search a user by username")
	@GetMapping("/user/search")
	public List<UserDTO> queryByUsername(@RequestParam(name = "username", required = true) String username) {
		return userService.queryByUsername(username);
	}

	@ApiOperation(value = "Update a single user")
	@PutMapping("/user")
	public UserDTO update(@RequestBody UserDTO userDTO) {
		return userService.update(userDTO);
	}

}
