package com.ioasys.empresasjava.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ioasys.empresasjava.DTO.MovieDTO;
import com.ioasys.empresasjava.service.MovieService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RequestMapping(value = "/api")
@Api(value = "API REST to list, update, save and delete movies")
@RestController
public class MovieController {

	@Autowired
	MovieService movieService;

	@ApiOperation(value = "Returns the list of all movies.")
	@GetMapping("/movie")
	public List<MovieDTO> getAllMovies() {
		List<MovieDTO> movies = movieService.getAllMovies();
		return movies;
	}
	
	@ApiOperation(value="Save a new movie")
	@PostMapping("/movie")
	public MovieDTO saveNewMovie(@RequestBody MovieDTO movieDTO) {
		return movieService.saveNewMovie(movieDTO);
	}
	
	@ApiOperation(value="Search a movie by filter")
	@GetMapping("/movie/search")
	public List<MovieDTO> getMoviesByFilter(
			@RequestParam(name = "director", required= false) String director,
			@RequestParam(name = "title", required= false) String title,
			@RequestParam(name = "genre", required= false) String genre,
			@RequestParam(name = "actor", required= false) String actor
			) {		
	    return movieService.getMoviesByFilter(director, title, genre, actor);
	}
	
}
