package com.ioasys.empresasjava.controller;

import java.util.List;

import javax.annotation.security.PermitAll;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ioasys.empresasjava.DTO.VoteDTO;
import com.ioasys.empresasjava.exception.UserNotFoundException;
import com.ioasys.empresasjava.service.VoteService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RequestMapping(value = "/api")
@Api(value = "API REST to list, update, save and delete votes")
@RestController
public class VoteController {

	@Autowired
	VoteService voteService;

	@ApiOperation(value = "Returns the list of all votes.")
	@GetMapping("/vote")
	@PermitAll
	public List<VoteDTO> getAllVotes() {
		List<VoteDTO> votes = voteService.getAllVotes();
		return votes;
	}
	
	@ApiOperation(value="Save a new vote")
	@PostMapping("/vote")
	@PermitAll
	public VoteDTO saveNewVote(@RequestBody VoteDTO voteDTO) {
		return voteService.saveNewVote(voteDTO);
	}
	
	@ApiOperation(value="Delete a vote by id")
	@DeleteMapping("/vote/{id}")
	@PermitAll
	public VoteDTO delete(@PathVariable Long id) throws UserNotFoundException {
		return voteService.delete(id);
	}
	
	@ApiOperation(value="Update a single vote")
	@PutMapping("/vote")
	@PermitAll
	public VoteDTO update(@RequestBody VoteDTO voteDTO) {
		return voteService.update(voteDTO);
	}
	
}
