package com.ioasys.empresasjava.converter;

import com.ioasys.empresasjava.DTO.MovieDTO;
import com.ioasys.empresasjava.DTO.UserDTO;
import com.ioasys.empresasjava.DTO.VoteDTO;
import com.ioasys.empresasjava.model.Movie;
import com.ioasys.empresasjava.model.User;
import com.ioasys.empresasjava.model.Vote;

public class DTOConverter {

	public static UserDTO convertUser(User user) {
		UserDTO userDTO = new UserDTO();
		userDTO.setId(user.getId());
		userDTO.setUsername(user.getUsername());
		userDTO.setFirstName(user.getFirstName());
		userDTO.setLastName(user.getLastName());
		userDTO.setEmail(user.getEmail());
		userDTO.setPassword(user.getPassword());
		userDTO.setType(user.getType());
		userDTO.setStatus(user.getStatus());
		userDTO.setDateRegister(user.getDateRegister());
		userDTO.setDateChange(user.getDateChange());
		return userDTO;
	}

	public static MovieDTO convertMovie(Movie movie) {
		MovieDTO movieDTO = new MovieDTO();
		movieDTO.setId(movie.getId());
		movieDTO.setTitle(movie.getTitle());
		movieDTO.setDescription(movie.getDescription());
		movieDTO.setDuration(movie.getDuration());
		movieDTO.setYear(movie.getYear());
		movieDTO.setCountry(movie.getCountry());
		movieDTO.setDirector(movie.getDirector());
		movieDTO.setGenre(movie.getGenre());
		movieDTO.setActor(movie.getActor());
		movieDTO.setStatus(movie.getStatus());
		movieDTO.setDateRegisterMovie(movie.getDateRegisterMovie());
		movieDTO.setDateChangeMovie(movie.getDateChangeMovie());
		return movieDTO;
	}

	public static VoteDTO convertVote(Vote vote) {
		VoteDTO voteDTO = new VoteDTO();
		voteDTO.setId(vote.getId());
		voteDTO.setRating(vote.getRating());
		voteDTO.setMovie(vote.getMovie());
		voteDTO.setUser(vote.getUser());
		voteDTO.setStatus(vote.getStatus());
		voteDTO.setDateRegisterVote(vote.getDateRegisterVote());
		voteDTO.setDateChangeVote(vote.getDateChangeVote());
		return voteDTO;
	}

}
